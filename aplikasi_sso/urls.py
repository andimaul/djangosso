"""aplikasi_sso URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url,include,static
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import path,include
from rest_framework import routers
from ssoapi.views import UserRegis,AppRegis,RoleRegis,PermissionRegis,LoginUser,LoginKoperasi,TestRole
from rest_framework_jwt.views import obtain_jwt_token
from django.views.decorators.csrf import csrf_exempt
from rest_framework_swagger.views import get_swagger_view


router = routers.DefaultRouter()
router.register(r'user-registrasi',UserRegis)
router.register(r'app-registrasi',AppRegis)
router.register(r'role-registrasi',RoleRegis)
router.register(r'permission-registrasi',PermissionRegis)
# router.register(r'testorle',TestRole)

# router.register(r'user',LoginUser)
schema_view = get_swagger_view(title='Django SSO')

urlpatterns = [
	# path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    # path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    path('grappelli/', include('grappelli.urls')), # grappelli URLS
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')), #login drf
    # path('login-app/',loginApp),
    url(r'^api/',include(router.urls)),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^swagger/', schema_view),
    path('login-koperasi/',LoginKoperasi.as_view()),
    path('user/',LoginUser.as_view()),
    path('test-role/',TestRole.as_view({'get':'list','post':'create'})),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    
#{'get':'list','post':'create'}