from django.apps import AppConfig


class SsoapiConfig(AppConfig):
    name = 'ssoapi'
