import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_rest_permission.settings')

import django

django.setup()
from .models import RoleUser


GROUPS = ['TNI Jenderal', 'Petani Jagung']
MODELS = ['RoleUser']

for group in GROUPS:
    new_group, created = RoleUser.objects.get_or_create(name=group)