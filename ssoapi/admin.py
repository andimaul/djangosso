from django.contrib import admin
from .models import UserApp,RoleUser,Aplikasi,ListPermission,Session
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
# Register your models here.

admin.site.register(UserApp)
admin.site.register(RoleUser)