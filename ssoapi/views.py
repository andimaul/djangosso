from django.shortcuts import render
from .models import UserApp,Aplikasi,RoleUser,ListPermission,Session
from rest_framework import viewsets,generics,status
from .serializers import UserSerializer,AppSerializer,RoleSerializer,PermissionSerializer,LoginSerializer,SessionSerializer
from rest_framework.authentication import SessionAuthentication, BasicAuthentication,BaseAuthentication
from rest_framework.permissions import IsAdminUser,AllowAny,IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.admin import TokenAdmin
from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework_jwt.settings import api_settings
from rest_framework.decorators import api_view,permission_classes,authentication_classes
from django.views.decorators.csrf import csrf_exempt,csrf_protect
import json,requests
import secrets
from rest_framework.test import RequestsClient
import django.middleware.csrf
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.views import APIView
from django.http import HttpResponse
from .permission import IsPetani, IsTNIStaff

# Create your views here.
class UserRegis(viewsets.ModelViewSet):
	serializer_class = UserSerializer
	queryset = UserApp.objects.all()
	permission_classes = [IsAdminUser]

class AppRegis(viewsets.ModelViewSet):
	serializer_class = AppSerializer
	queryset = Aplikasi.objects.all()
	permission_classes = [IsAdminUser]
	def create(self, request):
		namaapp = request.data["nama_aplikasi"]
		status = request.data["status_aplikasi"]
		token = secrets.token_hex()
		app = Aplikasi.objects.create(nama_aplikasi=namaapp,status_aplikasi=status,token_aplikasi=token)
		app.save()
		session = request.headers       
		return Response("Succes")

class RoleRegis(viewsets.ModelViewSet):
	serializer_class = RoleSerializer
	queryset = RoleUser.objects.filter(parent__isnull=True)
	permission_classes = [IsAdminUser]

class PermissionRegis(viewsets.ModelViewSet):
	serializer_class = PermissionSerializer
	queryset = ListPermission.objects.all()
	permission_classes = [IsAdminUser]


# @api_view(['POST'])
# @permission_classes([AllowAny])
# @authentication_classes([])
# def loginApp(request):
# 	try:
# 		pwd = request.data['pwd']
# 		phone = request.data['phone']
# 		user = request.data['username']
# 		# print(pwd,phone,user)
# 		# print(phone)
# 		obj = UserApp.objects.get(password=pwd,phone_user=phone,username=user)
			
# 		if obj :
			
# 			jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
# 			jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
# 			payload = jwt_payload_handler(obj)
# 			token = jwt_encode_handler(payload)
# 			return Response({"payload":payload,"token":token})
# 	except:
# 		return Response("Gagal")

class LoginUser(APIView):
	serializer_class = LoginSerializer
	queryset = UserApp.objects.all()
	permission_classes = []
	authentication_classes = []
	def get(self,request,format=None):
		response = request.session
		if "META" not in response:
			return Response("Silahkan Login")
		else:
			return Response(response)
	def post(self,request):
		try:
			pwd = request.data['password']
			phone = request.data['phone_user']
			user = request.data['username']
	

			obj = UserApp.objects.get(password=pwd,phone_user=phone,username=user)
			print(obj.pk)
			listper = ListPermission.objects.filter(user=obj.pk).values_list('aplikasi_id',flat=True)
			# print(listper)
			if 2 in listper and obj :
				
				jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
				jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
				payload = jwt_payload_handler(obj)
				# print(payload)
				token = jwt_encode_handler(payload)
				payload['token'] = token
				# print(token)
				# status_login = 1
				token_session = secrets.token_hex()
				session = Session.objects.create(status_login=1,token_session=token_session,list_permission_id=obj.pk)
				session.save()
				print(session)

				meta = {"code":status.HTTP_200_OK,
						"error":"",
						"message":"OK",
						"payload": {
						"data":[
							{
							"id":payload["user_id"],
							"exp":payload["exp"],
							}
							],
						"token":token,
						"username":payload["username"],
						"token_session":token_session,
							}
						}
				response = request.session
				response["META"]= json.dumps({"code":status.HTTP_200_OK,
						"error":"",
						"message":"OK",
						"payload": {
						"data":[
							{
							"id":payload["user_id"],
							"exp":payload["exp"],
							}
							],
						"token":token,
						"username":payload["username"]
							}
						})
				# namaapp = request.data["nama_aplikasi"]
				

				return Response({"test":response})
			elif obj:
				return Response("ID anda tidak punya akses di aplikasi ini")
		except:
			return Response("Gagal")
				# return Response({"payload":payload,"token":token})
		# except:
			# return Response("Gagal")


class LoginKoperasi(APIView):
	serializer_class = LoginSerializer
	queryset = UserApp.objects.all()
	permission_classes = [AllowAny]
	authentication_classes = []
	def post(self,request):
		try:
			pwd = request.data['password']
			phone = request.data['phone_user']
			user = request.data['username']
	

			obj = UserApp.objects.get(password=pwd,phone_user=phone,username=user)
			print(obj.pk)
			listper = ListPermission.objects.filter(user=obj.pk).values_list('aplikasi_id',flat=True)
			print(listper[0])
			if 1 in listper and obj :
				role = str(obj.role.parent)
				# print(obj.role.parent)
				jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
				jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
				payload = jwt_payload_handler(obj)
				# print(payload)
				token = jwt_encode_handler(payload)
				# print(token)
				payload['token'] = token
				print("token: "+str(token))
				# status_login = 1
				token_session = secrets.token_hex()
				# print("tokes: "+token_session)
				session = Session.objects.create(token_session=1,status_login=1,list_permission_id=2)
				# print(session)
				session.save()

				meta = {"code":status.HTTP_200_OK,
						"error":"",
						"message":"OK",
						"payload": {
						"data":[
							{
							"id":payload["user_id"],
							"exp":payload["exp"],
							}
							],
						"token":token,
						"username":payload["username"],
						"token_session":token_session,
							}
						}
				response = request.session

				response["META"] = meta

				return Response({"test":response})
			elif obj:
				return Response("ID anda tidak punya akses di aplikasi ini")
		except:
			return Response("Gagal")
	def get(self,request,format=None):
		response = request.session
		if "META" not in response:
			return Response("didnt work")
		else:
			return Response(response)



class TestRole(viewsets.ModelViewSet):
	serializer_class = UserSerializer
	queryset = UserApp.objects.all()
	permission_classes = [IsTNIStaff]
	# authentication_classes = [JSONWebTokenAuthentication]
	# def get_permissions(self):
	# 	permission_classes = []
	# 	if self.action == 'create':
	# 	    permission_classes = [IsPetani]
	# 	elif self.action == 'list':
	# 	    permission_classes = [AllowAny]
	# 	# elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
	# 	#     permission_classes = [IsLoggedInUserOrAdmin]
	# 	# elif self.action == 'destroy':
	# 	#     permission_classes = [IsLoggedInUserOrAdmin]
	# 	# return [permission() for permission in permission_classes]
	# 	return [permission() for permission in permission_classes]
