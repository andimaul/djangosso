from .models import RoleUser,UserApp
from rest_framework import permissions
from rest_framework.response import Response
from django.db.models import Q



class IsTNI(permissions.BasePermission):
    def has_permission(self, request, view):
        response = request.session
        # print(response["META"]["payload"]["data"][0]["id"])
        pk = response["META"]["payload"]["data"][0]["id"]
        role = UserApp.objects.filter(id=pk,role_id=2)
        print(role)
        if len(role)==0:
            print("tidak ada")
            return False
        print("ada")
        return True


class IsPetani(permissions.BasePermission):
     def has_permission(self, request, view):
        response = request.session
        # print(response["META"]["payload"]["data"][0]["id"])
        pk = response["META"]["payload"]["data"][0]["id"]
        role = UserApp.objects.filter(id=pk,role_id=4)
        print(role)
        if len(role)==0:
            print("tidak ada")
            return False
        print("ada")
        return True



class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.owner == request.user

class IsTNIStaff(permissions.BasePermission):
    message = 'not allowed.'
    def has_permission(self, request, view):
        try:
            response = request.session
            # print(response["META"]["payload"]["data"][0]["id"])
            pk = response["META"]["payload"]["data"][0]["id"]
            role = UserApp.objects.filter(id=pk,role_id__nama_role="TNI Staff")
            # print(role)
            if len(role)==0:
                print("tidak ada")
                return False
            message = "HALO"
            print("ada")
            return True
        except:
            return False
        # return request.method in permissions.SAFE_METHODS
        

    # def has_object_permission(self, request, view, obj):
    #     # Read permissions are allowed to any request,
    #     # so we'll always allow GET, HEAD or OPTIONS requests.
    #     if request.method in permissions.SAFE_METHODS:
    #         print(request.method)
    #         return True

    #     # Write permissions are only allowed to the owner of the snippet.
    #     return request.user