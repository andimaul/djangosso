from .models import UserApp,RoleUser,Aplikasi,ListPermission,Session
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserApp
		fields = '__all__'

class AppSerializer(serializers.ModelSerializer):
	class Meta:
		model = Aplikasi
		fields = ['nama_aplikasi','status_aplikasi','token_aplikasi']
		read_only_fields = ['token_aplikasi']

class RoleSerializer(serializers.ModelSerializer):
	children = RecursiveField(many=True,required=False)
	class Meta:
		model = RoleUser
		fields = ['id','parent','nama_role','status_role','children']

class PermissionSerializer(serializers.ModelSerializer):
	class Meta:
		model = ListPermission
		fields = '__all__'

class SessionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Session
		fields = '__all__'

class LoginSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserApp
		fields = ['phone_user','username','password']

class TokenAppSerializer(serializers.ModelSerializer):
	class Meta:
		model = Aplikasi
		fields = ['token_aplikasi']
		read_only_fields = ['token_aplikasi']