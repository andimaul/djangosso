from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
import uuid


STATUS = [
	(True,'Aktif'),
	(False,'Tidak Aktif')
]


# Create your models here.
class RoleUser(models.Model):
	parent = models.ForeignKey('self',related_name='children',null=True,blank=True,on_delete=models.PROTECT)
	nama_role = models.CharField(max_length=100,null=False)
	status_role = models.BooleanField(default=False,choices=STATUS)
	def __str__(self):
		return self.nama_role
		
class UserApp(models.Model):
	KELAMIN = [
	("Laki-laki","Laki-laki"),
	("Perempuan","Perempuan"),
	]
	# id_anggota = models.UUIDField(default=uuid.uuid4, editable=False)
	role = models.ForeignKey(RoleUser,on_delete=models.CASCADE)
	nama_user = models.CharField(max_length=100)
	phone_user = PhoneNumberField(null=False, blank=False, unique=True)
	alamat = models.CharField(max_length=100)
	alamat_ktp = models.CharField(max_length=100)
	kota_cd = models.CharField(max_length=100)
	provinsi_cd = models.CharField(max_length=100)
	no_ktp = models.CharField(max_length=100)
	email = models.EmailField()
	jenis_kelamin = models.CharField(max_length=100,default='Laki-laki',choices=KELAMIN)
	atas_nama = models.CharField(max_length=100)
	status_karyawan = models.BooleanField(default=False,choices=STATUS)
	username = models.CharField(max_length=100)
	password = models.CharField(max_length=50)
	def __str__(self):
		return('{},{},{}'.format(self.nama_user,self.role,self.phone_user))


class Aplikasi(models.Model):
	nama_aplikasi = models.CharField(max_length=100,null=False)
	status_aplikasi = models.BooleanField(default=False,choices=STATUS)
	token_aplikasi = models.CharField(max_length=100,null=False)
	def __str__(self):
		return self.nama_aplikasi

class ListPermission(models.Model):
	user = models.ForeignKey(UserApp, on_delete=models.CASCADE)
	# role = models.ForeignKey(RoleUser, on_delete=models.CASCADE)
	aplikasi = models.ForeignKey(Aplikasi, on_delete=models.CASCADE)
	def __repr__(self):
		return '{},{}'.format(self.user,self.aplikasi)

class Session(models.Model):
	list_permission = models.ForeignKey(ListPermission, on_delete=models.CASCADE)
	token_session = models.CharField(max_length=9999)
	status_login = models.BooleanField(default=False, choices=STATUS)


# class GroupRole(models.Model):
# 	role = models.CharField(max_length=100)
# 	role_level = models.IntegerField()